//PROGRAMA

//variables necesarias para el desarrollo del programa
//variables posicion y velocidad
float x;
float vx;
//variables para restringir al boton acelerar
float boton1x;
float boton11x;
float boton1y;
float boton11y;
//variables para restringir al boton frenar
float boton2x;
float boton22x;
float boton2y;
float boton22y;
//int tiempo1;
//int tiempo2;
//Inicio de la programacion
void setup(){
size(800,600);//tamaño de la ventana
smooth();
//condiciones iniciales
x=120;
vx=0;
boton1x=120;
boton11x=240;
boton1y=400;
boton11y=520;

boton2x=380;
boton22x=500;
boton2y=400;
boton22y=520;

}

void draw(){
paint();
//ecuacion que representa la posicion de "x" con respecto a la velocidad "vx"
x=x+vx; 
//condiciones para que al mantener presionado el boton acelerar aumente la velocidad
if (mouseX > boton1x && mouseX < boton11x && mouseY > boton1y && mouseY < boton11y){
  vx=1.3;
  //restricciones necesarias
  if (x>680){vx=0;
  }
}else {
  vx=-.3;
  if(x<120){vx=0;
  }
}
//condiciones para que al mantener presionado el boton frenar aumente la velocidad en sentido contrario
if (mouseX > boton2x && mouseX < boton22x && mouseY > boton2y && mouseY < boton22y){ 
    vx=-1.3;
   //restricciones necesarias
  if(x<120){
    vx=0;
  }
}


}

//Figuras geometricas que representan los botones para acelerar, frenar y para visualizar la velocidad mediante una barra
void paint(){
background(200);
fill(255);
rect(120,150,560,120);
strokeWeight(2);
line(x,150,x,270);

fill(#14FC00);
rect(120,400,120,120);

fill(#FC0D29);
rect(380,400,120,120);
}
